from django.views.decorators.http import require_http_methods
from django.shortcuts import render
from django.http import JsonResponse
from .models import Salesperson, Customer, Sale, AutomobileVO
from django.shortcuts import get_object_or_404
from .encoders import *
import json


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    """
    Lists the salespeople.
    Also, the code to create a new instance of salesperson.
    """
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the saleperson"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_salesperson_details(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False,
            )
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Salesperson does not exist"},
                status=400,
            )

    else:
        salesperson = Salesperson.objects.get(id=id)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    """
    Lists the customers.
    Also, the code to create a new instance of customer.
    """
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create the customer"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE", "GET"])
def api_customer_details(request, id):
    if request.method == "DELETE":
        try:
            customer = Customer.objects.get(id=id)
            customer.delete()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )

    else:
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

"""
Try and except blocks were added for automobile, salesperson, and
customer. If invalid data is entered for any of the fields, 400 messages
come back with statements about what is invalid.
"""

@require_http_methods(["GET", "POST"])
def api_list_sales(request, vin_vo_id=None):
    if request.method == "GET":
        if vin_vo_id is not None:
            sales = Sale.objects.filter(automobile=vin_vo_id)
        else:
            sales = Sale.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)


        try:
            automobile_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin number"},
                status=400,
            )

        try:
            salesperson_id = content["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
            content["salesperson"] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson id"},
                status=400,
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid customer id"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        automobile = sale.automobile
        automobile.sold = True
        automobile.save()

        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

"""
Although not required, I added the code below for the "GET" request to have
access to the details for each individual sale by id. I also did this for the models
above, but didn't really need them for any practical purposes. It was useful here to see
things like the status change on the sold boolean when that code was implemented
correctly. I didn't have access to it on the list all page, but it is available in
the details information that results from this addition. I know it's possible to add
more information into the list view via the encoder, but it's better to keep the list view
to a minimal amount.
"""

@require_http_methods(["DELETE", "GET"])
def api_sales_details(request, id):
    if request.method == "DELETE":
        try:
            sale = Sale.objects.get(id=id)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SaleEncoder,
                safe=False,
            )
        except Sale.DoesNotExist:
            return JsonResponse(
                {"message": "Sale does not exist"},
                status=400,
            )

    else:
        sale = Sale.objects.get(id=id)
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False,
        )

