from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = [
        "id",
        "employee_id",
        "first_name",
        "last_name",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "address",
        "first_name",
        "last_name",
        "phone_number"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold"
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        "id",
        "salesperson",
        "automobile",
        "price",
        "customer",
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, obj):

        return {"salesperson": {
            "id": obj.salesperson.id,
            "employee_id": obj.salesperson.employee_id,
            "first_name": obj.salesperson.first_name,
            "last_name": obj.salesperson.last_name
        }}

    def get_extra_data2(self, obj):
        return {"customer": obj.customer.id}

    """
Note: For some reason, when sales data was first fetched from the front end,
customer and automobile were already returning dictionaries with all the
needed data to populate the sales list table. See below:

sales = {
automobile: {import_href: '1B7GG23Y1NS526835', vin: '1B7GG23Y1NS526835', sold: false}
customer: {id: 2, address: 'PO Box 2022', first_name: 'Caleb', last_name: 'Wade',
           phone_number: '867-5309'}
salesperson:2
price:15000}

I was able to make a new sale with only the id from the data directly above,
but I had to add the extra information for salesperson using the dictionary
in the get extra data function above to complete the sales list and history tables.
    """
