from django.db import models


"""

The "double-unders", as they are colloquially refered to, were helpful early while
setting up the views. These allowed me to see the actual names or numbers associated
with input fields while checking the django admin page. For example, instead of
salesperson object(1), I could see the name associated with that object. This was
particularly helpful for automobile as it allowed me to see the vin numbers that resulted
from polling once it was functioning. Note: I also used the postgres database
to confirm that the vin numbers were populating the required database table.

I didn't add anything below the sales model, as I saw no practical purpose for it.
However, I did add "id" to the encoders to be able to reference each sale by an id
number. I added the id numbers to the sales list and history pages so that are shown
on the frontend.

"""

class Salesperson(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    employee_id = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class Customer(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address = models.TextField()
    phone_number = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Sale(models.Model):
    price = models.IntegerField()
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="sales",
        on_delete=models.CASCADE,
        null=False,
        default=None,
        blank=False
    )
    customer = models.ForeignKey(
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
        null=False,
        default=None,
        blank=False
    )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE,
        null=False,
        default=None,
        blank=False
    )
