# CarCar

Team:

* Person 1 - Austin S - Service
* Person 2 - Ryan Wade - Sales Microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

There are four models in the Sales microservice. The salesperson and customer models are really basic and have no foreign keys. The automobileVO model also doesn't have foreign keys, but it is more complicated as it is the place where the vin numbers from the automobiles (called "autos" in the inventory microservice) are stored in this microserice. The vin numbers are retrieved from the inventory via polling. These vin numbers are used with customer and saleperson data to make a new sale. This means that the sale model has foreign keys related to all three of the models already mentioned.

Note: I saw the inventory settings.py had "project-beta-inventory-api-1", but I wasn't able to get the polling function to work correctly until I added
"inventory-api" to the allowed hosts.
