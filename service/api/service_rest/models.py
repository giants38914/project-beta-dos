from django.db import models


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=200)
    sold = models.CharField(max_length=200, default="pending")
    import_href = models.CharField(max_length=200, default="", unique=True)


class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.PositiveIntegerField()


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=15, default="pending")
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
    )

    def full_name(self):
        return f"{self.first_name} {self.last_name}"
