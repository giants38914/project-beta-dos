from django.urls import path
from .views import (
    api_list_technicians,
    api_list_appointments,
    api_appointment_details,
    api_technician_details,
    api_service_history,
)


urlpatterns = [
    path(
        'technicians/',
        api_list_technicians,
        name="api_list_technicians"),
    path(
        'appointments/',
        api_list_appointments,
        name="api_list_appointments"),
    path(
        'appointments/<int:id>/',
        api_appointment_details,
        name="api_appointment_details"),
    path(
        'technicians/<int:id>/',
        api_technician_details,
        name="api_technician_details"),
    path(
        'servicehistory/',
        api_service_history,
        name="api_service_history"),
    ]


# api_finish_appointment,
# api_cancel_appointment,
# path(
#     'status/<int:id>/canceled/',
#     api_cancel_appointment,
#     name="cancel_appointment"),
# path(
#     'status/<int:id>/finished/',
#     api_finish_appointment,
#     name="finish_appointment"),
