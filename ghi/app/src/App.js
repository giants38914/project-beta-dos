import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespeopleList from './SalespeopleList';
import NewSalesperson from './NewSalesperson';
import SalesHistory from './SalesHistory';
import CustomerList from './CustomerList';
import ManufacturersList from './ManufacturersList';
import ModelsList from './ModelsList';
import NewCustomer from './NewCustomer';
import AutomobilesList from './AutomobilesList';
import ManufacturerForm from './ManufacturerForm';
import AppointmentList from './AppointmentList';
import NewAppointment from './NewAppointment';
import TechnicianList from './TechnicianList';
import NewTechnician from './NewTechnician';
import ServiceHistory from './ServiceHistory';
import NewSale from './NewSale';
import ModelForm from './ModelForm';
import AutomobileForm from './AutomobileForm';
import SalesList from './SalesList';



function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople" element={<SalespeopleList />} />
          <Route path="/salesperson/new" element={<NewSalesperson />} />
          <Route path="/customer/new" element={<NewCustomer />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sale/new" element={<NewSale />} />
          <Route path="/sold/history" element={<SalesHistory />} />
          <Route path="/manufacturers" element={<ManufacturersList />} />
          <Route path="/manufacturer/new" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelsList />} />
          <Route path="/model/new" element={<ModelForm />} />
          <Route path="/technicians" element={<TechnicianList />} />
          <Route path="/appointments" element={<AppointmentList />} />
          <Route path="/appointments/new" element={<NewAppointment />} />
          <Route path="/technicians/new" element={<NewTechnician />} />
          <Route path="/servicehistory/" element={<ServiceHistory />} />
          <Route path="/automobile/new" element={<AutomobileForm />} />
          <Route path="/automobiles" element={<AutomobilesList />} />



        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
