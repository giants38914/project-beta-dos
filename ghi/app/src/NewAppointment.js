import React, {useEffect, useState} from 'react';

function AppointmentForm(props) {

    const [dateTime, setDateTime] = useState("");
    const [reason, setReason] = useState("");
    const [status, setStatus] = useState("");
    const [vin, setVin] = useState("");
    const [customer, setCustomer] = useState("");
    const [technician, setTechnician] = useState("");
    const [technicians, setTechnicians] = useState([]);



        useEffect(() => {
        const fetchTechnicians = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians);
        }
    };

        fetchTechnicians();
}, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.date_time = dateTime;
        data.reason = reason;
        data.status = status;
        data.vin = vin;
        data.customer = customer;
        data.technician = technician;

        console.log(data);

        const appointmentUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
        method: "post",
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };

        const response = await fetch(appointmentUrl, fetchConfig);
        if (response.ok) {
        const newAppointment = await response.json();
        console.log(newAppointment);


        setDateTime('');
        // setTimeField('');
        setReason('');
        setStatus('');
        setVin('');
        setCustomer('');
        setTechnician('');


        }
    }

    const handleDateTimeChange = (event) => {
        const value = event.target.value;
        setDateTime(value);
    }


    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }

    const handleStatusChange = (event) => {
        const value = event.target.value;
        setStatus(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setTechnician(value);
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
                <div className="form-floating mb-3">
                <input value={dateTime} onChange={handleDateTimeChange} placeholder="DateTime" required type="date" name="date" id="date" className="form-control"/>
                <label htmlFor="dateTime">DateTime</label>
                </div>
                <div className="form-floating mb-3">
                <input value={reason} onChange={handleReasonChange} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
                </div>
                <div className="form-floating mb-3">
                <input value={status} onChange={handleStatusChange} placeholder="Status" required type="text" name="status" id="status" className="form-control"/>
                <label htmlFor="status">Status</label>
                </div>
                <div className="form-floating mb-3">
                <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin</label>
                </div>
                <div className="form-floating mb-3">
                <input value={customer} onChange={handleCustomerChange} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control"/>
                <label htmlFor="customer">Customer</label>
                </div>
                <div className="form-floating mb-3">
                    <select onChange={handleTechnicianChange} required name="technician" id="technician" className="form-select">
                        <option value="">Choose a technician</option>
                        {technicians.map(technician => {
                        return (
                        <option key={technician.id} value={technician.id}>
                        {technician.first_name + " " + technician.last_name}
                        </option>
                    );
                    })}
                    </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
}

export default AppointmentForm;
