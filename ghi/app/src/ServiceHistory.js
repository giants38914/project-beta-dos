import { useState, useEffect } from "react"


function ServiceHistory () {

    const [appointments, setAppointments] = useState([])
    const [history, setHistory] = useState([])


    const loadHistory = async () => {
        const resp = await fetch('http://localhost:8080/api/appointments/')

        if (resp.ok) {
            const data = await resp.json()
            setAppointments(data.appointments)
            setHistory(data.appointments)
        } else {
            console.error(resp)
        }
    }

    const handleClick = () => {
        const VIN = document.getElementById('vin search').value
        setHistory(appointments.filter(item => item.vin === VIN))
    }


    useEffect(() => {
        loadHistory();
    }, []);

    return (
        <div>
            <div className="input-group mb-3">
                <input className="form-control" placeholder="Search for VIN..." id='vinsearch' name='vinsearch'/>
                <button onClick={handleClick} className="btn btn-outline-secondary" id='button-addon2'>Search</button>
            </div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>DateTime</th>
                        <th>Reason</th>
                        <th>Status</th>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Technician</th>
                        <th>is VIP ?</th>
                    </tr>
                </thead>
                <tbody>
                    {history.map(appointment => {
                        return (
                        <tr key={appointment.id}>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>{appointment.vin}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.technician.first_name + " " + appointment.technician.last_name}</td>
                            <td>{appointment.vip}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory
