import React, { useEffect, useState } from "react";

function AutomobilesList(props) {
  const [autos, setAutos] = useState([]);
  const [sales, setSales] = useState([]);

  async function fetchAutos() {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos);
    }
  }

  async function fetchSales() {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSales(data.sales);
    }
  }

  useEffect(() => {
    fetchAutos();
    fetchSales();
  }, []);

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>VIN</th>
          <th>Color</th>
          <th>Year</th>
          <th>Model</th>
          <th>Sold</th>
        </tr>
      </thead>
      <tbody>
        {autos.map((auto) => {
          const sale = sales.find((s) => s.automobile.vin === auto.vin);
          const isSold = sale ? sale.automobile.sold : false;

          return (
            <tr key={auto.id}>
              <td>{auto.vin}</td>
              <td>{auto.color}</td>
              <td>{auto.year}</td>
              <td>{auto.model.name}</td>
              <td>{isSold ? "Yes" : "No"}</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobilesList;
