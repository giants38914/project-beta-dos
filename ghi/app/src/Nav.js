import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
              <NavLink className="nav-link" to="/salespeople">Salespeople List</NavLink>
              <NavLink className="nav-link" to="/salesperson/new">New Salesperson</NavLink>
              <NavLink className="nav-link" to="/customers">Customer List</NavLink>
              <NavLink className="nav-link"  to="/customer/new">New Customer</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/sales">Sales List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/sale/new">New Sale</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/sold/history">Sales History</NavLink>
              <NavLink className="nav-link" to="/manufacturers">Manufacturers List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/manufacturer/new">Manufacturer Form</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/models">Models List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/model/new">New Model</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/automobiles">Vehicle List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/automobile/new">New Automobile</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/appointments/new">New Appointment</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/technicians/new">New Technician</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/appointments/">Appointment List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/technicians/">Technician List</NavLink>
              <NavLink className="nav-link" aria-current="page" to="/servicehistory/">Service History</NavLink>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
