import React, { useEffect, useState } from "react";

function SalesList(props) {
  const [sales, setSales] = useState([]);

  async function fetchSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSales(data.sales);
    }
  }

  useEffect(() => {
    fetchSales();
  }, []);

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Sale ID</th>
          <th>Employee ID</th>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN Number</th>
          <th>Sale Price</th>
        </tr>
      </thead>
      <tbody>
        {sales.map((sale) => (
          <tr key={sale.id}>
            <td>{sale.id}</td>
            <td>{sale.salesperson.employee_id}</td>
            <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
            <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default SalesList;
