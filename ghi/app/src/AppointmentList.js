import React, { useEffect, useState } from "react";

function AppointmentsList(props) {


    const [appointments, setAppointments] = useState([]);

    async function fetchAppointments (){
        const response = await fetch("http://localhost:8080/api/appointments/");

        if (response.ok) {
        const data = await response.json();
        console.log(data);
        setAppointments(data.appointments);
        }
    };

    useEffect(() => {
        fetchAppointments();
    }, []);

    return (
        <table className="table table-success table-striped">
        <thead>
            <tr>
            <th>Date</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Vin</th>
            <th>Customer</th>
            <th>Technician</th>
            </tr>
        </thead>
        <tbody>
            {appointments.map((appointment) => (
                <tr key={appointment.id}>
                <td>{ appointment.date_time }</td>
                <td>{ appointment.reason }</td>
                <td>{ appointment.status }</td>
                <td>{ appointment.vin }</td>
                <td>{ appointment.customer }</td>
                <td>{`${appointment.technician.first_name} ${appointment.technician.last_name}`}</td>
                </tr>
            ))}
        </tbody>
    </table>
    );
}

export default AppointmentsList;
