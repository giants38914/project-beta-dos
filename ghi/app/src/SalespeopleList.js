import React, { useEffect, useState } from "react";

function SalespeopleList(props) {


  const [salespeople, setSalespeople] = useState([]);

  async function fetchSalespeople (){
    const response = await fetch("http://localhost:8090/api/salespeople/");

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSalespeople(data.salespeople);
    }
  };

  useEffect(() => {
    fetchSalespeople();
  }, []);

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Employee ID</th>
          <th>First Name</th>
          <th>Last Name</th>
          {/* <th>Employee Photo</th> */}
        </tr>
      </thead>
      <tbody>
        {salespeople.map(salesperson => {
          return (
            <tr key={salesperson.id}>
              <td>{ salesperson.employee_id }</td>
              <td>{ salesperson.first_name }</td>
              <td>{ salesperson.last_name }</td>

            {/* Adding a photo will be the same process as the idea for updating
            the manufacturer list to include the company logo. Both of these
            tasks can be done like adding the "picture_url" to the inventory's
            model list. */}

            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default SalespeopleList;
