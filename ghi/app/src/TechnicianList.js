import React, { useEffect, useState } from "react";

function TechniciansList(props) {


    const [technicians, setTechnicians] = useState([]);

    async function fetchTechnicians (){
        const response = await fetch("http://localhost:8080/api/technicians/");

        if (response.ok) {
        const data = await response.json();
        console.log(data);
        setTechnicians(data.technicians);
        }
    };

    useEffect(() => {
        fetchTechnicians();
    }, []);

    return (<>
        <h1>Technician List</h1>
        <table className="table table-success table-striped">
        <thead>
            <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            </tr>
        </thead>
        <tbody>
            {technicians.map(technician => {
            return (
                <tr key={technician.id}>
                <td>{ technician.first_name }</td>
                <td>{ technician.last_name }</td>
                <td>{ technician.employee_id }</td>
                </tr>
            );
            })}
        </tbody>
    </table>
    </> );
}

export default TechniciansList;
