import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

function ModelList(props) {


  const [models, setModels] = useState([]);

  async function fetchModels (){
    const response = await fetch("http://localhost:8100/api/models/");

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setModels(data.models);
    }
  };

  useEffect(() => {
    fetchModels();
  }, []);

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(model => {
          return (
            <tr key={model.id}>
              <td>{ model.name }</td>
              <td>{ model.manufacturer.name }</td>
              <td>
              <Link to={`/models/${model.id}`}>
                  <img
                    src={model.picture_url}
                    alt={model.name}
                    style={{ width: "250px", border: "4px solid #00b601" }}
                  />
                </Link>
                {/* I did some minor customization above to improve the overall look of the page. */}
                </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ModelList;
