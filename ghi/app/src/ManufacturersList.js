import React, { useEffect, useState } from "react";

function ManufacturersList(props) {


  const [manufacturers, setManufacturers] = useState([]);

  async function fetchManufacturers (){
    const response = await fetch("http://localhost:8100/api/manufacturers/");

    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchManufacturers();
  }, []);

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Name</th>
          {/* <th>Company Logo</th> */}
        </tr>
      </thead>
      <tbody>
        {manufacturers.map(manufacturer => {
          return (
            <tr key={manufacturer.id}>
              <td>{ manufacturer.name }</td>

              {/* <td>{ add link to image logo code here }</td>
              This will include adding to the model and updating the encoder,
              also changing the related form. Relatively easy task, it's very
              similar to how an image is added to the model list. */}


            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default ManufacturersList;
