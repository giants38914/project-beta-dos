import React, { useEffect, useState } from 'react';

function CustomerForm(props) {

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [address, setAddress] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");


  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.first_name = firstName;
    data.last_name = lastName;
    data.address = address;
    data.phone_number = phoneNumber;
    console.log(data);

    const customersUrl = 'http://localhost:8090/api/customers/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(customersUrl, fetchConfig);
    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);

      setFirstName('');
      setLastName('');
      setAddress('');
      setPhoneNumber('');
    }
  }

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  }

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  }

  const handleAddressChange = (event) => {
    const name = event.target.value;
    setAddress(name);
  }

  const handlePhoneNumberChange = (event) => {
    const name = event.target.value;
    setPhoneNumber(name);
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new customer</h1>
          <form onSubmit={handleSubmit} id="create-salesperson-form">
            <div className="form-floating mb-3">
              <input value={firstName} onChange={handleFirstNameChange} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control"/>
              <label htmlFor="first_name">First Name</label>
            </div>
            <div className="form-floating mb-3">
              <input value={lastName} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control"/>
              <label htmlFor="last_name">Last Name</label>
            </div>
            <div className="mb-3">
                    <textarea onChange={handleAddressChange}placeholder="Address" name="address" id="address" className="form-control" rows="3"></textarea>
                </div>
            <div className="form-floating mb-3">
              <input value={phoneNumber} onChange={handlePhoneNumberChange} placeholder="Phone Number" required type="text" name="phone_number" id="phone_number" className="form-control"/>
              <label htmlFor="city">Phone Number</label>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default CustomerForm;
