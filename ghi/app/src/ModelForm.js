import React, { useEffect, useState } from 'react';

function ModelForm(props) {
  const [manufacturers, setManufacturers] = useState([]);
  const [name, setName] = useState("");
  const [pictureUrl, setPictureUrl] = useState("");
  const [manufacturerId, setManufacturerId] = useState("");

  const fetchData = async () => {
    const url = 'http://localhost:8100/api/manufacturers/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {
      name: name,
      manufacturer_id: manufacturerId,
      picture_url: pictureUrl
    };

    const modelUrl = 'http://localhost:8100/api/models/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      const newModel = await response.json();
      console.log(newModel);

      setName('');
      setManufacturerId('');
      setPictureUrl('');
    }
  };

  const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
  };

  const handlePictureUrlChange = (event) => {
    const value = event.target.value;
    setPictureUrl(value);
  };

  const handleManufacturerIdChange = (event) => {
    const value = event.target.value;
    setManufacturerId(value);
  };

  return (
    <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new model listing</h1>
                <form onSubmit={handleSubmit} id="create-model-form">
                <div className="form-floating mb-3">
                    <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input value={pictureUrl} onChange={handlePictureUrlChange} required type="url" name="picture_url" id="picture_url" className="form-control"/>
                    <label htmlFor="picture_url">Picture URL</label>
                </div>
                <div className="mb-3">
                    <select onChange={handleManufacturerIdChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                    <option value="">Choose a manufacturer</option>
                {manufacturers.map(manufacturer => {
                return (
                  <option key={manufacturer.id} value={manufacturer.id}>
                  {manufacturer.name}
                  </option>
                );
              })}
            </select>
            </div>
                <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    </div>

  );
}

export default ModelForm;
