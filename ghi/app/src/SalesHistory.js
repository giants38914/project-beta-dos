import React, { useEffect, useState } from "react";

function SalesHistory(props) {
  const [sales, setSales] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState("");

  async function fetchSales() {
    const response = await fetch("http://localhost:8090/api/sales/");
    if (response.ok) {
      const data = await response.json();
      console.log(data);
      setSales(data.sales);
    }
  }

  useEffect(() => {
    fetchSales();
  }, []);

  const filteredSales = selectedSalesperson
    ? sales.filter((sale) =>
        `${sale.salesperson.first_name} ${sale.salesperson.last_name}`.toLowerCase().includes(selectedSalesperson.toLowerCase())
      )
    : sales;

  return (
    <table className="table table-success table-striped">
      <thead>
        <tr>
          <th>Sale ID</th>
          <th>Employee ID</th>
          <th>Salesperson</th>
          <th>Customer</th>
          <th>VIN Number</th>
          <th>Sale Price</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td colSpan="5">
            <label htmlFor="salesperson-select">Search by Name:</label>
            <select
              id="salesperson-select"
              value={selectedSalesperson}
              onChange={(event) => setSelectedSalesperson(event.target.value)}
            >
              <option value="">All Salespeople</option>
              {sales.map((sale) => (
                <option key={sale.id} value={`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}>
                  {`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}
                </option>
              ))}
            </select>
          </td>
        </tr>
        {filteredSales.map((sale) => (
          <tr key={sale.id}>
            <td>{sale.id}</td>
            <td>{sale.salesperson.employee_id}</td>
            <td>{`${sale.salesperson.first_name} ${sale.salesperson.last_name}`}</td>
            <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
            <td>{sale.automobile.vin}</td>
            <td>{sale.price}</td>
          </tr>
        ))}
      </tbody>
    </table>
  );
}

export default SalesHistory;
