import React, { useEffect, useState } from 'react';

function SaleForm(props) {
  const [customers, setCustomers] = useState([]);
  const [salespeople, setSalespeople] = useState([]);
  const [autos, setAutos] = useState([]);
  const [price, setPrice] = useState("");
  const [salesperson, setSalesperson] = useState("");
  const [customer, setCustomer] = useState("");
  const [automobile, setAutomobile] = useState("");

  useEffect(() => {
    const fetchSalespeople = async () => {
      const url = 'http://localhost:8090/api/salespeople/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salespeople);
      }
    };

    const fetchCustomers = async () => {
      const url = 'http://localhost:8090/api/customers/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      }
    };

    const fetchAutomobiles = async () => {
      const automobilesUrl = 'http://localhost:8100/api/automobiles/';
      const salesUrl = 'http://localhost:8090/api/sales/';
      const automobilesResponse = await fetch(automobilesUrl);
      const salesResponse = await fetch(salesUrl);
      if (automobilesResponse.ok && salesResponse.ok) {
        const automobilesData = await automobilesResponse.json();
        const salesData = await salesResponse.json();

        const soldVehicles = salesData.sales.map((sale) => sale.automobile.vin);
        const availableAutos = automobilesData.autos.filter(
          (auto) => !soldVehicles.includes(auto.vin)
        );

        setAutos(availableAutos);
      }
    };

    fetchSalespeople();
    fetchCustomers();
    fetchAutomobiles();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.price = price;
    data.salesperson = salesperson;
    data.customer = customer;
    data.automobile = automobile;

    console.log(data);

    const salesUrl = `http://localhost:8090/api/sales/`;
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };

    const response = await fetch(salesUrl, fetchConfig);
    if (response.ok) {
      const newSale = await response.json();
      console.log(response.json)
      console.log(newSale);

      setSalesperson('');
      setPrice('');
      setCustomer('');
      setAutomobile('');
    }
  };

  const handleSalespersonChange = (event) => {
    const value = event.target.value;
    setSalesperson(value);
  };

  const handlePriceChange = (event) => {
    const value = event.target.value;
    setPrice(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleAutomobileChange = (event) => {
    const value = event.target.value;
    setAutomobile(value);
  };

      return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new sale</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">

                        <div className="mb-3">
                        <select onChange={handleAutomobileChange} required name="automobile" id="automobile" className="form-select">
                          <option value="">Choose an automobile</option>
                          {autos.map((auto) => (
                            <option key={auto.vin} value={auto.vin}>
                              {auto.vin}
                            </option>
                          ))}
                        </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                            <option value="">Choose a salesperson</option>
                        {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                            {salesperson.first_name + " " + salesperson.last_name}
                            </option>
                        );
                    })}
                        </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                            <option value="">Choose a customer</option>
                        {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                            {customer.first_name + " " + customer.last_name}
                            </option>
                        );
                    })}
                </select>
                </div>
                        <div className="form-floating mb-3">
                            <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price</label>
                        </div>

                    <button className="btn btn-primary">Create</button>
                    </form>
                </div>
                </div>
            </div>
        </div>

      );
    }


    export default SaleForm;
